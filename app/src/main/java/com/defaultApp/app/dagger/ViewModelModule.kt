package com.defaultApp.app.dagger

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.defaultApp.app.ui.login.LoginViewModel
import com.defaultApp.app.ui.main.MainViewModel
import com.defaultApp.app.ui.moviedetail.MovieDetailViewModel
import com.defaultApp.app.ui.movielist.MovieListViewModel
import com.defaultApp.app.ui.splashscreen.SplashScreenViewModel
import com.defaultApp.app.ui.webview.WebviewViewModel
import com.defaultApp.app.util.AppVMFactory
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Suppress("unused")
@Module
abstract class ViewModelModule {
    @Binds
    abstract fun bindViewModelFactory(factory: AppVMFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(SplashScreenViewModel::class)
    abstract fun bindSplashScreenViewModel(viewModel: SplashScreenViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    abstract fun bindMainViewModel(viewModel: MainViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(LoginViewModel::class)
    abstract fun bindLoginViewModel(viewModel: LoginViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MovieListViewModel::class)
    abstract fun bindMovieListViewModel(viewModel: MovieListViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MovieDetailViewModel::class)
    abstract fun bindMovieDetailViewModel(viewModel: MovieDetailViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(WebviewViewModel::class)
    abstract fun bindWebviewViewModel(viewModel: WebviewViewModel): ViewModel

}