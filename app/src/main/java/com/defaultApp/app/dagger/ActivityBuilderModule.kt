package com.defaultApp.app.dagger

import com.defaultApp.app.ui.login.LoginActivity
import com.defaultApp.app.ui.main.MainActivity
import com.defaultApp.app.ui.moviedetail.MovieDetailActivity
import com.defaultApp.app.ui.movielist.MovieListActivity
import com.defaultApp.app.ui.splashscreen.SplashScreenActivity
import com.defaultApp.app.ui.webview.WebviewActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
internal abstract class ActivityBuildersModule {
    @ContributesAndroidInjector
    internal abstract fun bindSplashScreenActivity(): SplashScreenActivity

    @ContributesAndroidInjector
    internal abstract fun bindMainActivity(): MainActivity

    @ContributesAndroidInjector
    internal abstract fun bindLoginActivity(): LoginActivity

    @ContributesAndroidInjector
    internal abstract fun bindMovieListActivity(): MovieListActivity

    @ContributesAndroidInjector
    internal abstract fun bindMovieDetailActivity(): MovieDetailActivity

    @ContributesAndroidInjector
    internal abstract fun bindWebviewActivity(): WebviewActivity
}