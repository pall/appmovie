package com.defaultApp.app.ui.movielist

import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.defaultApp.app.Constants
import com.defaultApp.app.R
import com.defaultApp.app.databinding.ItemListMovieBinding
import com.defaultApp.app.model.Genre
import com.defaultApp.app.model.Result
import com.defaultApp.app.ui.main.MainAdapter
import com.defaultApp.app.ui.moviedetail.MovieDetailActivity
import com.defaultApp.app.util.OnLoadMoreListener

class MovieListAdapter(
    val activity: MovieListActivity,
    private val recyclerView: RecyclerView
): RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    var data: MutableList<Result> = arrayListOf()
    var lastVisibleItem = 0
    var totalItem = 0
    var isLoading = false
    //    var isComplete = false
    var onLoadMoreListener: OnLoadMoreListener? = null
    val visibleThresHold = 10

    init {
        val linearLayoutManager = recyclerView.layoutManager as LinearLayoutManager
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)

                totalItem = linearLayoutManager.itemCount
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition()
                if (!isLoading && totalItem < lastVisibleItem + visibleThresHold && dy > 0) {
                    if (onLoadMoreListener != null) {
                        onLoadMoreListener!!.onLoadMore()
                    }
                    isLoading = true
                }
            }
        })
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ItemListMovieHolder(
            ItemListMovieBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )
    }

    override fun getItemCount(): Int {
        Log.d("Debug-mov","data :"+data.size)
        return data.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is ItemListMovieHolder -> holder.bind(data[position])
        }
    }

    inner class ItemListMovieHolder(itemView: ItemListMovieBinding) :
        RecyclerView.ViewHolder(itemView.root) {
        val binding = itemView
        fun bind(datas: Result){
            Glide.with(activity)
                .load(Constants.BaseImgUrl+datas.poster_path)
                .placeholder(R.drawable.no_img)
                // .diskCacheStrategy(DiskCacheStrategy.NONE)
                // .skipMemoryCache(true)
                .error(R.drawable.no_img)
                .into(binding.imgMovie)

            binding.layoutContent.setOnClickListener {
                activity.startActivity(
                    Intent(activity, MovieDetailActivity::class.java)
                        .putExtra("idMovie",datas.id.toString())
                        .putExtra("title_toolbar",datas.title)
                )
            }

            binding.tvJudul.text = datas.title
            binding.tvDesc.text = datas.overview
            binding.tvRelease.text = "Release Date :"+datas.release_date
        }
    }

    fun setOnloadMoreListener(onLoadMoreListener: OnLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener
    }

}