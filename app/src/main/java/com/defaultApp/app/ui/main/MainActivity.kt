package com.defaultApp.app.ui.main

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.defaultApp.app.R
import com.defaultApp.app.databinding.ActivityGenreBinding
import com.defaultApp.app.ui.base.BaseActivity
import javax.inject.Inject

class MainActivity : BaseActivity() {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var viewModel: MainViewModel
    private lateinit var binding: ActivityGenreBinding
    lateinit var adapter: MainAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_genre)
        viewModel = initViewModel(MainViewModel::class.java, viewModelFactory)
        setToolbar("Category")
        initAdapter()
//        setUpRecyclerSkeleton(binding.rvGenre, R.layout.item_list_genre,10)
        binding.pbGenre.visibility = View.VISIBLE
        viewModel.init()
//        recyclerSkeleton.showSkeleton()
        binding.viewModel = viewModel
        initObserver()
    }

    private fun initAdapter() {
        adapter = MainAdapter(this)
        binding.rvGenre.adapter = adapter
    }

    private fun initObserver() {
        viewModel.dataGenre.observe(this, Observer {
            binding.pbGenre.visibility = View.GONE
            if (it != null) {
                for (i in it.genres.indices){
                    Log.d("debug-find","data ::"+it.genres[i])
                    adapter.data.add(it.genres[i])
                    adapter.notifyDataSetChanged()
                }
            }
        })
    }

    override fun onStart() {
        super.onStart()
    }

    override fun onResume() {
        super.onResume()
    }
}
