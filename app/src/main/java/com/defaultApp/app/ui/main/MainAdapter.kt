package com.defaultApp.app.ui.main

import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.defaultApp.app.databinding.ItemListGenreBinding
import com.defaultApp.app.model.Genre
import com.defaultApp.app.ui.movielist.MovieListActivity

class MainAdapter(
    val activity: MainActivity
): RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    var data: MutableList<Genre> = arrayListOf()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ItemListGenreHolder(
            ItemListGenreBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is ItemListGenreHolder -> holder.bind(data[position])
        }
    }
    inner class ItemListGenreHolder(itemView: ItemListGenreBinding) :
        RecyclerView.ViewHolder(itemView.root) {
        val binding = itemView
        fun bind(datas: Genre){
            binding.data = datas
            binding.container.setOnClickListener {
                activity.startActivity(Intent(activity, MovieListActivity::class.java)
                    .putExtra("idGenre",datas.id.toString())
                    .putExtra("title_toolbar",datas.name)
                )
            }
        }
    }

}
