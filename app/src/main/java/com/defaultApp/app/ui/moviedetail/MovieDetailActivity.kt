package com.defaultApp.app.ui.moviedetail

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.defaultApp.app.Constants
import com.defaultApp.app.R
import com.defaultApp.app.databinding.ActivityMovieDetailBinding
import com.defaultApp.app.ui.base.BaseActivity
import com.defaultApp.app.ui.movielist.MovieListActivity
import com.defaultApp.app.ui.webview.WebviewActivity
import com.defaultApp.app.util.OnLoadMoreListener
import javax.inject.Inject

class MovieDetailActivity: BaseActivity() {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var binding: ActivityMovieDetailBinding
    private lateinit var viewModel: MovieDetailViewModel
    lateinit var adapter: MovieReviewAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_movie_detail)
        viewModel = initViewModel(MovieDetailViewModel::class.java, viewModelFactory)
        setToolbar(intent.getStringExtra("title_toolbar").toString())
        initAdapter()
        viewModel.idMovie = intent.getStringExtra("idMovie").toString()
        viewModel.init()
        viewModel.page = 1
        viewModel.initReviews()
        initObserver()
        initListener()
        setUpRecyclerSkeleton(binding.rvReview,R.layout.item_list_review, 6)
        recyclerSkeleton.showSkeleton()
    }

    private fun initListener() {
        binding.imgPlay.setOnClickListener {
            startActivity(
                Intent(this, WebviewActivity::class.java)
                .putExtra("idMovie",viewModel.idMovie)
            )
        }


        adapter.setOnloadMoreListener(object : OnLoadMoreListener {
            override fun onLoadMore() {
                viewModel.initReviews()
            }
        })
    }

    private fun initObserver() {
        viewModel.dataMovieDetail.observe(this, Observer {
            Log.d("Debug-film","data :::"+it)
            if (it != null) {
                Glide.with(this)
                    .load(Constants.BaseImgUrl+it.backdrop_path)
                    .placeholder(R.drawable.no_img)
                    // .diskCacheStrategy(DiskCacheStrategy.NONE)
                    // .skipMemoryCache(true)
                    .error(R.drawable.no_img)
                    .into(binding.imgContent)

                binding.tvJudul.text = it.title
                binding.tvDesc.text = it.overview
                binding.tvRelease.text = "Release Data :"+it.release_date
            }
        })

        viewModel.dataReviews.observe(this, Observer {
            if (viewModel.page == 1) {
                recyclerSkeleton.showOriginal()
            }
            if (it != null) {
                adapter.isLoading = false
                if (viewModel.page == 1){
                    adapter.data.clear()
                    adapter.notifyDataSetChanged()
                }
                val tmpSize = adapter.data.size
                if (viewModel.page == 1 && it.results.isEmpty()) {
                    binding.tvNoReviews.visibility = View.VISIBLE
                } else {
                    binding.tvNoReviews.visibility = View.GONE
                }
                for (i in it.results.indices) {
                    adapter.data.add(it.results[i])
                    adapter.notifyItemInserted(tmpSize)
                }
                viewModel.page++
            }
        })
    }

    private fun initAdapter() {
        adapter = MovieReviewAdapter(this, binding.rvReview)
        binding.rvReview.adapter = adapter
    }
}