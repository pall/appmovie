package com.defaultApp.app.ui.moviedetail

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.defaultApp.app.databinding.ItemListReviewBinding
import com.defaultApp.app.model.ResultX
import com.defaultApp.app.util.OnLoadMoreListener

class MovieReviewAdapter (
    val activity: MovieDetailActivity,
    private val recyclerView: RecyclerView
): RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    var data: MutableList<ResultX> = arrayListOf()
    var lastVisibleItem = 0
    var totalItem = 0
    var isLoading = false
    //    var isComplete = false
    var onLoadMoreListener: OnLoadMoreListener? = null
    val visibleThresHold = 10

    init {
        val linearLayoutManager = recyclerView.layoutManager as LinearLayoutManager
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)

                totalItem = linearLayoutManager.itemCount
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition()
                if (!isLoading && totalItem < lastVisibleItem + visibleThresHold && dy > 0) {
                    if (onLoadMoreListener != null) {
                        onLoadMoreListener!!.onLoadMore()
                    }
                    isLoading = true
                }
            }
        })
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ItemListReviewsHolder(
            ItemListReviewBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is ItemListReviewsHolder -> holder.bind(data[position])
        }
    }

    inner class ItemListReviewsHolder(itemView: ItemListReviewBinding) :
        RecyclerView.ViewHolder(itemView.root) {
        val binding = itemView
        fun bind(datas: ResultX){
            binding.profileName.text = datas.author
            binding.tvReview.text = datas.content
        }
    }

    fun setOnloadMoreListener(onLoadMoreListener: OnLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener
    }
}