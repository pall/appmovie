package com.defaultApp.app.ui.webview

import android.os.Bundle
import android.util.Log
import android.view.View
import android.webkit.WebChromeClient
import android.webkit.WebSettings
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.defaultApp.app.R
import com.defaultApp.app.databinding.ActivityWebviewBinding
import com.defaultApp.app.ui.base.BaseActivity
import com.defaultApp.app.ui.main.MainViewModel
import javax.inject.Inject

class WebviewActivity: BaseActivity() {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var viewModel: WebviewViewModel
    private lateinit var binding: ActivityWebviewBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_webview)
        viewModel = initViewModel(WebviewViewModel::class.java, viewModelFactory)
        viewModel.idMovie = intent.getStringExtra("idMovie").toString()
        binding.pbWebview.visibility = View.VISIBLE
        viewModel.init()
        initObserver()
    }

    private fun initObserver() {
        viewModel.dataVideo.observe(this, Observer {
            binding.pbWebview.visibility = View.GONE
            if (it != null) {
                for (i in it.results.indices) {
                    val data = it.results[i]
                    if (data.type.equals("Trailer",true)){
                        viewModel.keyUrl = data.key
                        initView()
                    }
                }
            }
        })
    }

    private fun initView() {
        val webSettings: WebSettings = binding.webView.settings
        webSettings.javaScriptEnabled = true
        val youtubeVideoId = viewModel.keyUrl
        val html = "<html><body><iframe width=\"100%\" height=\"100%\" src=\"https://www.youtube.com/embed/$youtubeVideoId?autoplay=1\" frameborder=\"0\" allowfullscreen></iframe></body></html>"
        binding.webView.loadData(html, "text/html", "utf-8")
    }
}