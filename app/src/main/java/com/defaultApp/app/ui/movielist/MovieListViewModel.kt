package com.defaultApp.app.ui.movielist

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.defaultApp.app.Constants
import com.defaultApp.app.cache.PreferencesHelper
import com.defaultApp.app.model.DataGenre
import com.defaultApp.app.model.DataMovie
import com.defaultApp.app.repository.UserRepository
import com.defaultApp.app.ui.base.BaseViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

class MovieListViewModel @Inject constructor(
    val userRepository: UserRepository
) : BaseViewModel() {
    private val movie = MutableLiveData<DataMovie>()
    val dataMovie: LiveData<DataMovie> get() = movie
    var idGenre: String = ""
    var page: Int = 0

    fun init() {
        viewModelScope.launch {
            movie.value = userRepository.getListMovie(Constants.ApiKeys,idGenre,page)
        }
    }
}