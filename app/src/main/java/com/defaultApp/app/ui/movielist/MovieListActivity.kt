package com.defaultApp.app.ui.movielist

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import com.defaultApp.app.R
import com.defaultApp.app.databinding.ActivityMovieListBinding
import com.defaultApp.app.ui.base.BaseActivity
import com.defaultApp.app.ui.main.MainAdapter
import com.defaultApp.app.ui.main.MainViewModel
import com.defaultApp.app.util.OnLoadMoreListener
import javax.inject.Inject

class MovieListActivity : BaseActivity() {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var binding: ActivityMovieListBinding
    private lateinit var viewModel: MovieListViewModel
    lateinit var adapter: MovieListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_movie_list)
        viewModel = initViewModel(MovieListViewModel::class.java, viewModelFactory)
        setToolbar(intent.getStringExtra("title_toolbar").toString())
        initAdapter()
        viewModel.idGenre = intent.getStringExtra("idGenre").toString()
        viewModel.page = 1
        viewModel.init()
        initObserver()
        initListener()
    }

    private fun initListener() {
        adapter.setOnloadMoreListener(object : OnLoadMoreListener {
            override fun onLoadMore() {
                binding.progressBar.visibility = View.VISIBLE
                viewModel.init()
            }
        })
    }

    private fun initObserver() {
        viewModel.dataMovie.observe(this, Observer {
            binding.progressBar.visibility = View.GONE
            if (it != null) {
                adapter.isLoading = false
                if (viewModel.page == 1){
                    adapter.data.clear()
                    adapter.notifyDataSetChanged()
                }
                val tmpSize = adapter.data.size
                for (i in it.results.indices) {
                    adapter.data.add(it.results[i])
                    adapter.notifyItemInserted(tmpSize)
                }
                viewModel.page++
            }
        })
    }

    private fun initAdapter() {
        binding.rvMoviewList.layoutManager = GridLayoutManager(this, 2)
        adapter = MovieListAdapter(this, binding.rvMoviewList)
        binding.rvMoviewList.adapter = adapter
    }
}