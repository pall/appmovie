package com.defaultApp.app.ui.moviedetail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.defaultApp.app.Constants
import com.defaultApp.app.cache.PreferencesHelper
import com.defaultApp.app.model.DataMovieDetail
import com.defaultApp.app.model.DataReviews
import com.defaultApp.app.repository.UserRepository
import com.defaultApp.app.ui.base.BaseViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

class MovieDetailViewModel @Inject constructor(
    val userRepository: UserRepository,
    val preferencesHelper: PreferencesHelper
) : BaseViewModel() {
    private val detailMovie = MutableLiveData<DataMovieDetail>()
    val dataMovieDetail: LiveData<DataMovieDetail> get() = detailMovie
    var idMovie: String = ""
    var page: Int = 0

    fun init() {
        viewModelScope.launch {
            detailMovie.value = userRepository.getMovieDetail(idMovie,Constants.ApiKeys)
        }
    }

    private val reviews = MutableLiveData<DataReviews>()
    val dataReviews: LiveData<DataReviews> get() = reviews

    fun initReviews() {
        viewModelScope.launch {
            reviews.value = userRepository.getMovieReviews(idMovie,Constants.ApiKeys,page)
        }
    }
}