package com.defaultApp.app.ui.webview

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.defaultApp.app.Constants
import com.defaultApp.app.cache.PreferencesHelper
import com.defaultApp.app.model.DataVideo
import com.defaultApp.app.repository.UserRepository
import com.defaultApp.app.ui.base.BaseViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

class WebviewViewModel @Inject constructor(
    val userRepository: UserRepository,
    val preferencesHelper: PreferencesHelper
) : BaseViewModel() {
    private val video = MutableLiveData<DataVideo>()
    val dataVideo: LiveData<DataVideo> get() = video
    var keyUrl: String = ""
    var idMovie: String = ""

    fun init() {
        viewModelScope.launch {
            video.value = userRepository.getVideo(idMovie,Constants.ApiKeys)
        }
    }
}