package com.defaultApp.app.ui.main

import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.viewModelScope
import com.defaultApp.app.Constants
import com.defaultApp.app.api.model.Resource
import com.defaultApp.app.cache.PreferencesHelper
import com.defaultApp.app.model.DataGenre
import com.defaultApp.app.repository.UserRepository
import com.defaultApp.app.ui.base.BaseViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

class MainViewModel @Inject constructor(
    val userRepository: UserRepository,
    val preferencesHelper: PreferencesHelper
) : BaseViewModel() {
    private val genres = MutableLiveData<DataGenre>()
    val dataGenre: LiveData<DataGenre> get() = genres

    fun init() {
        viewModelScope.launch {
            genres.value = userRepository.getListGenre()
        }
    }
}