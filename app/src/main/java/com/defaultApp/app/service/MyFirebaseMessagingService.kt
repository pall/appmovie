package com.defaultApp.app.service

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Color
import android.media.RingtoneManager
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.core.content.ContextCompat
import com.defaultApp.app.R
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage

class MyFirebaseMessagingService : FirebaseMessagingService() {

    override fun onNewToken(p0: String) {
        super.onNewToken(p0)
        Log.d("debug-token", "data :" + p0)
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage){
        Log.d("debug-pesan", "Dikirim dari: ${remoteMessage.from}")

//        if (remoteMessage.data.isNotEmpty()) {
//            notifId = remoteMessage.data["notificationid"]
//            imgUrl = remoteMessage.data["image_url"]
//            type = remoteMessage.data["type"]
//            /*if ( *//* Check if data needs to be processed by long running job *//*true) {
//                // For long-running tasks (10 seconds or more) use Firebase Job Dispatcher.
//                scheduleJob()
//            } else {
//                // Handle message within 10 seconds
//                handleNow()
//            }*/
//        }

        if (remoteMessage.notification != null) {
            showNotification(remoteMessage.notification?.title ?: "", remoteMessage.notification?.body ?: "")
        }
    }
    private fun showNotification(title: String?, body: String?) {
//        var bitmapIcon: Bitmap? = null
//        if (imgUrl.isNotNullAndNotEmpty()){
//            bitmapIcon = getBitmapFromURL(imgUrl)
//        }
//        val intent = Intent(this, NewSplashScreenActivity::class.java)
//        intent.putExtra("type", type)
        /*val pendingIntent = PendingIntent.getActivity(
            this,
            0,
            intent,
            PendingIntent.FLAG_UPDATE_CURRENT
        )*/
        // var pendingIntent: PendingIntent? = null
//        val pendingIntent = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.) {
//            PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_IMMUTABLE)
//        } else {
//            PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
//        }
//        val intentBroadcast = Intent("IDEALNOTIF")
//        intentBroadcast.putExtra("type", type)
//        sendBroadcast(intentBroadcast)

        val notificationBuilder = NotificationCompat.Builder(this, getString(R.string.channel_id))
            .setSmallIcon(R.mipmap.ic_launcher)
            .setColor(ContextCompat.getColor(this, R.color.teal_800))
            .setContentTitle(title)
            .setContentText(body)
            .setAutoCancel(true)
//            .setContentIntent(pendingIntent)
            .setStyle(
                NotificationCompat.BigTextStyle()
                    .bigText(body)
            )
//            .setLargeIcon(bitmapIcon)
            .setPriority(NotificationCompat.PRIORITY_MAX)
            .setGroupAlertBehavior(NotificationCompat.GROUP_ALERT_SUMMARY)
            .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
            .setVibrate(longArrayOf(1000, 1000))
            .setLights(Color.BLUE, 3000, 3000)
        // .build()

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val descriptionText = "slot"
            val importance = NotificationManager.IMPORTANCE_HIGH

            val idealChannel = NotificationChannel(
                getString(R.string.channel_id),
                "General",
                importance
            ).apply {
                description = descriptionText
                setShowBadge(true)
            }
            notificationManager.createNotificationChannel(idealChannel)
        }

        with(NotificationManagerCompat.from(this)) {
            // notificationId is a unique int for each notification that you must define
            notify(0, notificationBuilder.build())
           /* if (notifId.isNotNullAndNotEmpty()){
                notify(notifId!!.toInt(), notificationBuilder.build())
            }else{
                notify(0, notificationBuilder.build())
            }*/
        }

        /*val intent = Intent(this, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val pendingIntent = PendingIntent.getActivity(this, 0, intent,
            PendingIntent.FLAG_ONE_SHOT)

        val soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder = NotificationCompat.Builder(this)
            .setSmallIcon(R.mipmap.ic_launcher)
            .setContentTitle(title)
            .setContentText(body)
            .setAutoCancel(true)
            .setSound(soundUri)
            .setContentIntent(pendingIntent)

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.notify(0, notificationBuilder.build())*/
    }
}
