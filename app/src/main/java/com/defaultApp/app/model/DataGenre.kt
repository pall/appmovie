package com.defaultApp.app.model

import com.google.gson.annotations.SerializedName

data class DataGenre(
    @SerializedName("genres")
    val genres: List<Genre>
)

data class Genre(
    @SerializedName("id")
    val id: Int,
    @SerializedName("name")
    val name: String
)