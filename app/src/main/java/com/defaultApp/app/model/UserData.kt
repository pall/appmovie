package com.defaultApp.app.model

import com.google.gson.annotations.SerializedName

data class UserData(
    @SerializedName("divisi")
    var divisi: String? ="",
    @SerializedName("foto")
    var foto: String? ="",
    @SerializedName("hak_akses")
    var kodeKaryawan: String? ="",
    @SerializedName("kode_lokasi")
    var kodeLokasi: String? ="",
    @SerializedName("last_login")
    var lastLogin: String? ="",
    @SerializedName("login_now")
    var loginNow: String? ="",
    @SerializedName("nama")
    var nama: String? ="",
    @SerializedName("pekerjaan")
    var pekerjaan: String? ="",
    @SerializedName("user_id")
    var userId: String? =""
)