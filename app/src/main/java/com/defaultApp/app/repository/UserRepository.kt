package com.defaultApp.app.repository

import android.provider.ContactsContract.Data
import android.util.Log
import androidx.lifecycle.LiveData
import com.defaultApp.app.api.ApiService
import com.defaultApp.app.api.ApiSuccessResponse
import com.defaultApp.app.api.model.BaseDataResponse
import com.defaultApp.app.api.model.BaseResponse
import com.defaultApp.app.api.model.RequestHeader
import com.defaultApp.app.api.model.Resource
import com.defaultApp.app.cache.PreferencesHelper
import com.defaultApp.app.model.*
import com.defaultApp.app.util.AppExecutors
import com.defaultApp.app.util.NetworkBoundResource
import io.reactivex.Single
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UserRepository @Inject constructor(
    private val appExecutors: AppExecutors,
    private val preferencesHelper: PreferencesHelper,
    private val APIService: ApiService,
    private val requestHeaders: RequestHeader,
    private val localUserData: LocalUserData
) {

    fun getLocalUserData(): Single<UserData> {
        return preferencesHelper.getAccountRx()
    }

    suspend fun getListGenre(): DataGenre {
        return APIService.getListGenre()
    }

    suspend fun getListMovie(apiKey: String, idGenre: String, page: Int): DataMovie {
        return APIService.getListMovie(apiKey,idGenre,page)
    }

    suspend fun getMovieDetail(idMovie: String, apiKey: String): DataMovieDetail {
        return APIService.getMovieDetail(idMovie,apiKey)
    }

    suspend fun getMovieReviews(idMovie: String, apiKey: String, page: Int): DataReviews {
        return APIService.getMovieReviews(idMovie,apiKey,page)
    }

    suspend fun getVideo(idMovie: String, apiKey: String): DataVideo {
        return APIService.getVideo(idMovie,apiKey)
    }
}