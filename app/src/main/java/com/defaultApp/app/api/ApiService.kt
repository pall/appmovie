package com.defaultApp.app.api

import androidx.lifecycle.LiveData
import com.defaultApp.app.api.model.BaseDataResponse
import com.defaultApp.app.api.model.BaseListResponse
import com.defaultApp.app.api.model.BaseResponse
import com.defaultApp.app.model.*
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.*


interface ApiService {
    @GET("genre/movie/list")
    suspend fun getListGenre(): DataGenre

    @GET("discover/movie")
    suspend fun getListMovie(
        @Query("api_key") apiKey: String,
        @Query("with_genres") genre: String,
        @Query("page") page: Int
    ): DataMovie

    @GET("movie/{idMovie}")
    suspend fun getMovieDetail(
        @Path("idMovie") idMovie: String,
        @Query("api_key") apiKey: String
    ): DataMovieDetail

    @GET("movie/{idMovie}/reviews")
    suspend fun getMovieReviews(
        @Path("idMovie") idMovie: String,
        @Query("api_key") apiKey: String,
        @Query("page") page: Int
    ): DataReviews

    @GET("movie/{idMovie}/videos")
    suspend fun getVideo(
        @Path("idMovie") idMovie: String,
        @Query("api_key") apiKey: String
    ): DataVideo
}