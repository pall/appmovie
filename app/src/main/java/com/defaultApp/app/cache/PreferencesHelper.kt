package com.defaultApp.app.cache

import android.content.Context
import android.content.SharedPreferences
import com.defaultApp.app.model.UserData
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class PreferencesHelper @Inject constructor(private val context: Context) {
    companion object {
        private const val keyPackageName = "originalnest"
        private const val keyUserId = "userId"
        private const val keyUserName = "userName"
        private const val keyEmployeeCode = "employeeCode"
        private const val keyLocationCode = "locationCode"
        private const val keyDivision = "division"
        private const val keyJob = "job"
        private const val keyRememberMe = "rememberMe"
        private const val keyProfilePicture = "profilePicture"
        private const val userName = "userName"
        private const val timeLimited = "timeLimited"
        private const val provider = "provider"
        private const val cheatEngine = "cheatEngine"
        private const val autoWinRate = "autoWinRate"
        private const val buySpin = "buySpin"
        private const val firebasePushToken = "FirebasePushToken"
    }

    private val atPref: SharedPreferences =
        context.getSharedPreferences(keyPackageName, Context.MODE_PRIVATE)

    var rememberMe: Boolean
        get() = atPref.getBoolean(keyRememberMe, false)
        set(rememberMe) = atPref.edit().putBoolean(keyRememberMe, rememberMe).apply()

    fun saveAccount(userData: UserData) {
        atPref.edit().putBoolean(keyRememberMe, true).apply()
        atPref.edit().putString(keyUserId, userData.userId ?: "").apply()
        atPref.edit().putString(keyUserName, userData.nama ?: "").apply()
        atPref.edit().putString(keyEmployeeCode, userData.kodeKaryawan ?: "").apply()
        atPref.edit().putString(keyLocationCode, userData.kodeLokasi ?: "").apply()
        atPref.edit().putString(keyDivision, userData.divisi ?: "").apply()
        atPref.edit().putString(keyJob, userData.pekerjaan ?: "").apply()
        atPref.edit().putString(keyProfilePicture, userData.foto ?: "").apply()
    }

    fun saveProfilePicture(link: String) {
        atPref.edit().putString(keyProfilePicture, link).apply()
    }

    fun getAccountRx(): Single<UserData> {
        val user = UserData(
            foto = atPref.getString(keyProfilePicture, null),
            userId = atPref.getString(keyUserId, null),
            nama = atPref.getString(keyUserName, null),
            kodeKaryawan = atPref.getString(keyEmployeeCode, null),
            kodeLokasi = atPref.getString(keyLocationCode, null),
            divisi = atPref.getString(keyDivision, null),
            pekerjaan = atPref.getString(keyJob, null)
        )
        return Single.just(user)
    }

   /* fun saveDataActivated(dataActive: DataActivated) {
        atPref.edit().putString(userName, dataActive.userName ?: "").apply()
        atPref.edit().putString(provider, dataActive.provider ?: "").apply()
        atPref.edit().putString(cheatEngine, dataActive.cheatEngine ?: "").apply()
        atPref.edit().putString(autoWinRate, dataActive.autoWinRate ?: "").apply()
        atPref.edit().putString(buySpin, dataActive.buySpin ?: "").apply()
    }

    fun getDataActivated(): Single<DataActivated> {
        val data = DataActivated(
            userName = atPref.getString(userName, null),
            provider = atPref.getString(provider, null),
            cheatEngine = atPref.getString(cheatEngine, null),
            autoWinRate = atPref.getString(autoWinRate, null),
            buySpin = atPref.getString(buySpin, null)
        )
        return Single.just(data)
    }*/

    fun clearSession() {
//        val fcm = getUserToken()
        atPref.edit().clear().apply()
//        saveUserToken(fcm)
//        val i = Intent(context, LoginActivity::class.java)
//        i.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
//        context.startActivity(i)
    }

    fun setTimeLimited(time: Int) {
        atPref.edit().putInt(timeLimited, time).apply()
    }

    fun getTimeLimited(): Int {
        return atPref.getInt(timeLimited, 0)
    }

    fun setFirebasePushToken(keyTmp: String?) {
        atPref.edit().putString(firebasePushToken , keyTmp ?: "").apply()
    }
    val getFirebasePushToken: String?
        get() = atPref.getString(firebasePushToken , "")

}